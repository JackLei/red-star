local currentIdVal = redis.call('GET', KEYS[1])
if not currentIdVal
    then currentIdVal = 0
end
local newIdVal = currentIdVal + 1
redis.call('SET', KEYS[1], newIdVal);
return newIdVal