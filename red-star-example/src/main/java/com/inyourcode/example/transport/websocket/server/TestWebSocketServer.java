/*
 * Copyright (c) 2021 The red-star Project
 *
 * Licensed under the Apache License, version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at:
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.inyourcode.example.transport.websocket.server;

import com.inyourcode.core.transport.netty.websocket.NettyWebsocketAcceptor;
import com.inyourcode.core.transport.session.IllegalHandlerException;
import com.inyourcode.core.transport.session.SessionHandlerMapping;
import com.inyourcode.example.service.handler.TestProtoHandler;

import java.util.Arrays;

/**
 * @author JackLei
 */
public class TestWebSocketServer {

    public static void main(String[] args) throws InterruptedException, IllegalHandlerException {
        SessionHandlerMapping.mappingHandler(Arrays.asList(new TestProtoHandler()));
        NettyWebsocketAcceptor nettyWebsocketAcceptor = new NettyWebsocketAcceptor(8080);
        nettyWebsocketAcceptor.start();
    }
}
