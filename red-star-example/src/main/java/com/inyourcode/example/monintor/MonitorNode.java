/*
 * Copyright (c) 2021 The red-star Project
 *
 * Licensed under the Apache License, version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at:
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.inyourcode.example.monintor;

import com.inyourcode.core.db.redis.RedisConfig;
import com.inyourcode.core.monitor.MonitorServer;
import org.springframework.beans.factory.annotation.Configurable;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.scheduling.annotation.EnableScheduling;

/**
 * @author JackLei
 */
@Configurable
@Configuration
@EnableScheduling
@ComponentScan(basePackages = "${app.scanpackages}")
@Import(RedisConfig.class)
public class MonitorNode {
    @Value("${monitor.console.port}")
    private int consolePort;

    @Bean
    MonitorServer monitorServer() throws InterruptedException {
        MonitorServer monitorServer = new MonitorServer(consolePort);
        monitorServer.start();
        return monitorServer;
    }
}

