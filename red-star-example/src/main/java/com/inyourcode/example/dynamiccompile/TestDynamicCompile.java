/*
 * Copyright (c) 2021 The red-star Project
 *
 * Licensed under the Apache License, version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at:
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.inyourcode.example.dynamiccompile;

import com.inyourcode.core.compile.DynamicCompiler;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

/**
 * @author JackLei
 */
public class TestDynamicCompile {

    public static void main(String[] args) throws NoSuchMethodException, InvocationTargetException, IllegalAccessException {
        String program = "" +
                "public class CodeGenTest {\n" +
                "  public static void main(String[] args) {\n" +
                "    System.out.println(\"Hello World, from a generated program!\");\n" +
                "  }\n" +
                "}\n";

        Class dynamicClass = DynamicCompiler.compileBySourceCode("CodeGenTest", program);
        if (dynamicClass == null) {
            System.out.println("compile error");
            return;
        }

        Method main = dynamicClass.getMethod("main", String[].class);
        main.invoke(null, new Object[]{null});
    }
}
