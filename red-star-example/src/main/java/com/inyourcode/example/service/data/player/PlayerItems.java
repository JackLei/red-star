package com.inyourcode.example.service.data.player;

import com.inyourcode.core.db.api.Entity;

import java.util.HashMap;
import java.util.Map;

/**
 * @author JackLei
 */
@Entity(key = "playerItem")
public class PlayerItems{
    private Map<Integer, PlayerItem> itemMap = new HashMap<>();

    public void addItem(int modelId, int add) {
        PlayerItem playerItem = itemMap.get(modelId);
        if (playerItem == null) {
            playerItem = new PlayerItem();
            playerItem.setModelId(modelId);
        }
        int old = playerItem.getCount();
        playerItem.setCount(old + add);
    }

    public Map<Integer, PlayerItem> getItemMap() {
        return itemMap;
    }
}
