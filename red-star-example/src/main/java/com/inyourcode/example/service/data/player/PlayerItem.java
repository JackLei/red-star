package com.inyourcode.example.service.data.player;

/**
 * @author JackLei
 */
public class PlayerItem {
    private int modelId;
    private int count;

    public int getModelId() {
        return modelId;
    }

    public void setModelId(int modelId) {
        this.modelId = modelId;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }
}
