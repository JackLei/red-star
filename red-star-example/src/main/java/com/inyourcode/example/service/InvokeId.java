package com.inyourcode.example.service;

public interface InvokeId {
    int C2L_TEST = 1;
    int L2C_TEST = 2;

    int C2L_Login = 1000;
    int L2C_Login = 1001;
    int C2L_Logout = 1002;
    int L2C_Logout = 1003;
    int C2L_ProtoTest = 1004;
    int L2C_ProtoTest = 1005;
}
