package com.inyourcode.example.service.data.player;

import com.inyourcode.core.db.api.Entity;

import java.util.HashMap;
import java.util.Map;

/**
 * @author JackLei
 */
@Entity(key = "playerInfo")
public class PlayerBaseInfo {
    private long id;
    private String name;
    private int level;
    private int exp;
    private Map<Integer, Long> properties = new HashMap<>();
    private Map<Integer, Long> resources = new HashMap<>();

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getLevel() {
        return level;
    }

    public void setLevel(int level) {
        this.level = level;
    }

    public int getExp() {
        return exp;
    }

    public void setExp(int exp) {
        this.exp = exp;
    }

    public Map<Integer, Long> getProperties() {
        return properties;
    }

    public void setProperties(Map<Integer, Long> properties) {
        this.properties = properties;
    }

    public Map<Integer, Long> getResources() {
        return resources;
    }

    public void setResources(Map<Integer, Long> resources) {
        this.resources = resources;
    }
}
