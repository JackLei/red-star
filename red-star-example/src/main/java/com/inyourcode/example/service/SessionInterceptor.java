package com.inyourcode.example.service;

import com.inyourcode.core.transport.session.HandlerWrapper;
import com.inyourcode.core.transport.session.api.ISessionInterceptor;
import com.inyourcode.core.transport.session.api.Session;

public class SessionInterceptor implements ISessionInterceptor {

    @Override
    public boolean intercept(HandlerWrapper processerWrapper, Object message, Session session) {

        return false;
    }
}
