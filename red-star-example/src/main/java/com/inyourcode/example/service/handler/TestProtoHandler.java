/*
 * Copyright (c) 2021 The red-star Project
 *
 * Licensed under the Apache License, version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at:
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.inyourcode.example.service.handler;

import com.inyourcode.core.transport.session.ResponseContext;
import com.inyourcode.core.transport.session.RequestContext;
import com.inyourcode.core.transport.session.api.NoAuthRequest;
import com.inyourcode.core.transport.session.api.RequestMapping;
import com.inyourcode.core.transport.session.api.Session;
import com.inyourcode.example.protobuf.LoginProto;
import com.inyourcode.example.service.IErrorCode;
import com.inyourcode.example.service.InvokeId;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;

/**
 * @author JackLei
 */
@Controller
public class TestProtoHandler {
    private static final Logger LOGGER = LoggerFactory.getLogger(TestProtoHandler.class);

    @NoAuthRequest
    @RequestMapping(builder = LoginProto.C2L_ProtoTest.class, invokeId = InvokeId.C2L_ProtoTest)
    public ResponseContext test(Session session, RequestContext requestContext){
        LoginProto.C2L_ProtoTest msg = requestContext.getData();
        LOGGER.info("receive message from client:{} ", msg.getToken());
        LoginProto.L2C_ProtoTest.Builder result = LoginProto.L2C_ProtoTest.newBuilder();
        result.setCode(IErrorCode.SUCCESS);

        ResponseContext responseContext = new ResponseContext(InvokeId.L2C_ProtoTest, requestContext.getTraceId(), result, requestContext.getSerializerCode());
        return responseContext;
    }

    @NoAuthRequest
    @RequestMapping(builder = LoginProto.L2C_ProtoTest.class, invokeId = InvokeId.L2C_ProtoTest)
    public ResponseContext respTest(Session session, ResponseContext requestContext){
        LoginProto.L2C_ProtoTest msg = requestContext.getData();
        LOGGER.info("recive message from server:{} ", msg.getCode());
        return null;
    }
}
