package com.inyourcode.example.service.handler;

import com.inyourcode.core.transport.session.api.MessageHolder;
import com.inyourcode.core.transport.session.RequestContext;
import com.inyourcode.core.transport.session.api.RequestMapping;
import com.inyourcode.core.transport.session.api.Session;
import com.inyourcode.example.service.InvokeId;
import com.inyourcode.example.service.data.C2LTest;
import com.inyourcode.example.service.data.L2CTest;
import org.springframework.stereotype.Controller;

@Controller
public class TestTransportHandler {

    @RequestMapping(builder = C2LTest.class, invokeId = InvokeId.C2L_TEST)
    public MessageHolder test(Session session, RequestContext requestContext){
        C2LTest msg = requestContext.getData();
        System.out.println("recive message from client : "+ msg.data);
        L2CTest result = new L2CTest();
        result.data= "pong";
//        session.write(MessageHolder.create(InvokeId.L2C_TEST, result));
        return null;
    }

    @RequestMapping(builder = L2CTest.class, invokeId = InvokeId.L2C_TEST)
    public MessageHolder respTest(Session session, RequestContext requestContext){
        L2CTest msg = requestContext.getData();
        System.out.println("recive message from server : "+ msg.data);
        return null;
    }
}
