/*
 * Copyright (c) 2021 The red-star Project
 *
 * Licensed under the Apache License, version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at:
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.inyourcode.example.service.data;

import com.inyourcode.example.service.data.player.PlayerBaseInfo;
import com.inyourcode.example.service.data.player.PlayerItems;


/**
 * @author JackLei
 */
public class MSGBuilder {

    public static MSG_PlayerInfo build(PlayerBaseInfo playerBaseInfo) {
        MSG_PlayerInfo resp = new MSG_PlayerInfo();
        if (playerBaseInfo == null) {
            return resp;
        }
        resp.id = playerBaseInfo.getId();
        resp.name = playerBaseInfo.getName();
        resp.level = playerBaseInfo.getLevel();
        resp.exp = playerBaseInfo.getExp();
        resp.properties = playerBaseInfo.getProperties();
        resp.resources = playerBaseInfo.getResources();
        return resp;
    }

    public static MSG_PlayerItem build(PlayerItems playerItems) {
        MSG_PlayerItem resp = new MSG_PlayerItem();
        if (playerItems == null) {
            return resp;
        }
        playerItems.getItemMap().forEach((k, v)->{
            MSG_PlayerItemSub sub = new MSG_PlayerItemSub();
            sub.modelId = v.getModelId();
            sub.count = v.getCount();

            resp.itemMap.put(sub.modelId, sub);
        });
        return resp;
    }
}


