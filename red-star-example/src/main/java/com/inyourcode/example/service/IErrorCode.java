package com.inyourcode.example.service;

public interface IErrorCode {
    int SUCCESS = 0;
    int LOGIN_NODE_NOT_FOUND = -1;
    int LOGIN_JWT_AUTH_ERROR = -2;
    int LOGIN_PLAYER_DATA_GEN_ERROR = -3;
    int LOGIN_RESET = -4;
    int LOGIN_REPLACE_ACCOUNT = -5;

}
