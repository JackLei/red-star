package com.inyourcode.example.service.lobby;

import com.inyourcode.core.event.api.EventClazz;
import com.inyourcode.core.event.api.EventMethod;
import com.inyourcode.core.transport.session.api.Session;
import com.inyourcode.example.service.data.player.PlayerBaseInfo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

/**
 * @author JackLei
 */
@Component
@EventClazz
public class LobbyEventListener {
    private static final Logger LOGGER  = LoggerFactory.getLogger(LobbyEventListener.class);

    @EventMethod(name = "login", priority = 1)
    public void initPlayerData(Session session){
        LOGGER.info("on login event 1");
        PlayerBaseInfo playerBaseInfo = session.getContextData(PlayerBaseInfo.class);
        if (playerBaseInfo == null) {
            playerBaseInfo = new PlayerBaseInfo();
            playerBaseInfo.setId(session.getId());
            playerBaseInfo.setExp(0);
            playerBaseInfo.setLevel(100);
            playerBaseInfo.setName("tester[" + session.getId() + "]");
            session.putContextData(playerBaseInfo);
        }
    }

    @EventMethod(name = "login", priority = 2)
    public void test(Session session){
        LOGGER.info("on login event 2");
    }
}
