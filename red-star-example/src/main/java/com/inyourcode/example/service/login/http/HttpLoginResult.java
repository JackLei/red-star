package com.inyourcode.example.service.login.http;

public class HttpLoginResult {
    public int code;
    public String token;
    public String host;
    public int port;
}
