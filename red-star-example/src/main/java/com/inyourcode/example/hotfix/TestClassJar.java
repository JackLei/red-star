/*
 * Copyright (c) 2021 The red-star Project
 *
 * Licensed under the Apache License, version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at:
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.inyourcode.example.hotfix;

import com.inyourcode.core.spring.ClassHelper;

import java.io.IOException;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

/**
 * @author JackLei
 */
public class TestClassJar {

    public static void main(String[] args) throws IOException {
        Set<String> classList = new HashSet<>();
        classList.add("com/inyourcode/example/transport/rpc/ServiceTestImpl.class");

        Map<String, byte[]> clazzBytesMap = ClassHelper.readClassBytes("hotfix-path/hotfix-test.jar", classList);

        clazzBytesMap.forEach((name, bytes) -> {
            System.out.println("name:" + name + ",bytes len: " + bytes.length +"\n");
        });

    }
}
