package com.inyourcode.example.cluster;
/**
 一、框架要实现的需求：
 1、所有节点可以水平扩展,可以动态添加、移除节点，玩家无感知
 2、数据库配置，一个Lobby集群，对应一组Redis、mongo, 公共的数据也会有单独的redis、mongo存储（在例子中只使用redis）
 3、理论上可以实现大区架构的需求
 二、集群配置：
 1、Lobby集群组，每组3个子节点
 2、Web集群一组，每组3个子节点
 3、Login集群一组，每组3个子节点
应用场景：
 1、玩家经过sdk验证后，先经过登陆服，在登陆服验证通过后，在一个Lobby集群里选择一个Lobby节点




 */