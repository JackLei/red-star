/*
 * Copyright (c) 2021 The red-star Project
 *
 * Licensed under the Apache License, version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at:
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.inyourcode.example.cluster.client;

import com.inyourcode.core.util.JWTUtil;
import com.inyourcode.core.transport.http.HttpClientUtil;
import com.inyourcode.core.spring.HttpClientCondition;
import com.inyourcode.core.spring.JWTCondition;
import com.inyourcode.core.transport.session.BasicTcpConnector;
import org.springframework.beans.factory.annotation.Configurable;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.*;
import org.springframework.scheduling.annotation.EnableScheduling;


/**
 * @author JackLei
 */
@Configurable
@Configuration
@PropertySource(value = {"classpath:client1.properties"})
@EnableScheduling
@ComponentScan(basePackages = "${app.scanpackages}")
public class ClientNode {
    @Value("${jwt.secretKey}")
    private String jwtSecretKey;
    @Value("${jwt.issuer}")
    private String jwtIssuer;
    @Value("${jwt.expireTimeMillis}")
    private long jwtExpireTimeMillis;

    @Value("${tcp.listen.ip}")
    private String tcpListenIp;
    @Value("${tcp.listen.port}")
    private int tcpListenPort;
    @Value("${tcp.client.addr}")
    private String tcpClientAddr;

    @Conditional(JWTCondition.class)
    @Bean
    JWTUtil jwtUtil(){
        return new JWTUtil(jwtSecretKey, jwtExpireTimeMillis, jwtIssuer);
    }

    @Bean
    BasicTcpConnector tcpConnector() throws InterruptedException {
        BasicTcpConnector tcpConnector = new BasicTcpConnector(1);
        return tcpConnector;
    }

    @Conditional(HttpClientCondition.class)
    @Bean
    HttpClientUtil httpClient() {
        return new HttpClientUtil();
    }

}

