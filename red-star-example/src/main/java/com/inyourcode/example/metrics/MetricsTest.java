/*
 * Copyright (c) 2021 The red-star Project
 *
 * Licensed under the Apache License, version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at:
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.inyourcode.example.metrics;

import com.codahale.metrics.ConsoleReporter;
import com.codahale.metrics.CsvReporter;
import com.codahale.metrics.Histogram;
import com.codahale.metrics.Meter;
import com.codahale.metrics.MetricRegistry;
import com.codahale.metrics.Slf4jReporter;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.Locale;
import java.util.Random;
import java.util.concurrent.TimeUnit;

/**
 * @author JackLei
 */
public class MetricsTest {
    private static final MetricRegistry METRIC_REGISTRY = new MetricRegistry();
    private static final Meter requests = METRIC_REGISTRY.meter("requests");
    private static final Histogram responseSizes = METRIC_REGISTRY.histogram("response-sizes");

    public static void main(String[] args) throws InterruptedException, IOException {
        consoleReport();
        return;
    }

    private static void consoleReport() throws InterruptedException {
        new Thread( ()->{
            while (true) {
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }

                handleRequest();
            }
        }, "T#REQUEST").start();


        ConsoleReporter reporter = ConsoleReporter.forRegistry(METRIC_REGISTRY)
                .convertRatesTo(TimeUnit.SECONDS)
                .convertDurationsTo(TimeUnit.MILLISECONDS)
                .build();
        reporter.start(30, TimeUnit.SECONDS);

        Thread.sleep(Integer.MAX_VALUE);
    }

    private static void csvReport() throws InterruptedException, IOException {
        new Thread( ()->{
            while (true) {
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }

                handleRequest();
            }
        }, "T#REQUEST").start();


        String property = System.getProperty("user.dir");
        File csvReporter = new File(property + File.separator + "CsvReporter");
        if (!csvReporter.exists()) {
            csvReporter.mkdirs();
        }


        final CsvReporter reporter = CsvReporter.forRegistry(METRIC_REGISTRY)
                .formatFor(Locale.US)
                .convertRatesTo(TimeUnit.SECONDS)
                .convertDurationsTo(TimeUnit.MILLISECONDS)
                .build(csvReporter);

        reporter.start(1, TimeUnit.SECONDS);

        Thread.sleep(Integer.MAX_VALUE);
    }

    private static void slf4jReport() throws InterruptedException, IOException {
        new Thread( ()->{
            while (true) {
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }

                handleRequest();
            }
        }, "T#REQUEST").start();

        final Slf4jReporter reporter = Slf4jReporter.forRegistry(METRIC_REGISTRY)
                .outputTo(LoggerFactory.getLogger(MetricsTest.class))
                .convertRatesTo(TimeUnit.SECONDS)
                .convertDurationsTo(TimeUnit.MILLISECONDS)
                .build();
        reporter.start(1, TimeUnit.MINUTES);
        Thread.sleep(Integer.MAX_VALUE);
    }


    public static void handleRequest() {
        requests.mark();
        Random random = new Random();
        int nextInt = random.nextInt(111);
        responseSizes.update(nextInt);
    }
}
