/*
 * Copyright (c) 2021 The red-star Project
 *
 * Licensed under the Apache License, version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at:
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.inyourcode.excel.api;

import com.inyourcode.excel.ExportContext;
import com.inyourcode.excel.ExportException;

/**
 * 导出抽象
 * @author JackLei
 */
public interface IExporter {

    /**
     * 导出前检查
     * @param context
     * @return 返回true，才会执行{@link #export(ExportContext)}方法
     */
    default boolean check(ExportContext context) throws ExportException{
        return true;
    }

    /**
     *
     * @param context 导出需要的数据上下文
     * @throws ExportException
     */
    void export(ExportContext context) throws ExportException;
}
