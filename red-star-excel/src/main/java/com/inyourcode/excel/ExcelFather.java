/*
 * Copyright (c) 2021 The excel-father Project
 *
 * Licensed under the Apache License, version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at:
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.inyourcode.excel;

import com.google.common.base.Strings;
import com.inyourcode.excel.api.IExporter;
import com.inyourcode.excel.iml.CSharpExporter;
import com.inyourcode.excel.iml.JavaExporter;
import com.inyourcode.excel.iml.JsonExporter;
import com.inyourcode.excel.model.SheetDataModel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.io.InputStream;
import java.util.HashSet;
import java.util.Map;
import java.util.Properties;
import java.util.Set;

/**
 * 启动类
 * @author JackLei
 * @version 2020-01-31-15:01
 */
public class ExcelFather {

    static Logger LOGGER = LoggerFactory.getLogger(ExcelFather.class);
    private static final Set<IExporter> exporterList = new HashSet<>();
    private static final String KEY_EXCEL_PATH = "excel.file.path";

    /**
     * 初始化导出器
     */
    private static void initExporter() {
        exporterList.add(new JsonExporter());
        exporterList.add(new JavaExporter());
        exporterList.add(new CSharpExporter());
    }

    public static void main(String[] args) throws ExportException {

        Properties properties  = new Properties();
        InputStream propInputStream = ExcelFather.class.getClassLoader().getResourceAsStream("red-star-excel.properties");
        try {
            properties.load(propInputStream);
        } catch (IOException e) {
            throw new ExportException("load properties fail" ,e);
        }

        //初始化导出器
        initExporter();
        String excelPath = properties.getProperty(KEY_EXCEL_PATH);
        if (Strings.isNullOrEmpty(excelPath)) {
            throw new ExportException("Please set excel file path");
        }

        //读取excel数据模型
        Map<String, SheetDataModel> sheetDataModelMap = ExcelMonther.scanExcelData(excelPath);
        ExportContext exportContext = ExportContext.create(properties, sheetDataModelMap);

        //导出
        for (IExporter exporter : exporterList) {
            if (!exporter.check(exportContext)) {
                continue;
            }

            exporter.export(exportContext);
        }
    }

}
