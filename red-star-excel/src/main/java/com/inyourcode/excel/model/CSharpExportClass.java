package com.inyourcode.excel.model;

import java.util.*;

/**
 * Java类数据模型
 * @author JackLei
 */
public class CSharpExportClass {
    /** 包名 */
    private String packageName;
    /** 类名 */
    private String javaClassName;
    private Set<CSharpExportField> fields = new LinkedHashSet<>();

    public CSharpExportClass(String packageName, String javaClassName) {
        this.packageName = packageName;
        this.javaClassName = javaClassName.substring(0 ,1).toUpperCase() + javaClassName.substring(1);
    }

    public String getPackageName() {
        return packageName;
    }

    public String getJavaClassName() {
        return javaClassName;
    }

    public Set<CSharpExportField> getFields() {
        return fields;
    }

    public void setPackageName(String packageName) {
        this.packageName = packageName;
    }

    public void setJavaClassName(String javaClassName) {
        this.javaClassName = javaClassName;
    }

    public void setFields(Set<CSharpExportField> fields) {
        this.fields = fields;
    }

    /**
     * 属性数据模型
     */
    public static class CSharpExportField {
        /** 属性类型 */
        private String fieldType;
        /** 属性名字 */
        private String fieldName;
        /** 方法名 */
        private String methodNamePrefix;
        /** 注释 */
        private String comment;
        /* json 序列化类型**/
        private int serializerType;

        public CSharpExportField(String fieldType, String fieldName, String comment, int serializerType) {
            this.fieldType = fieldType;
            this.fieldName = fieldName;
            this.comment = comment;
            this.methodNamePrefix = this.fieldName.substring(0, 1).toUpperCase() + this.fieldName.substring(1);
            this.serializerType = serializerType;
        }

        public String getFieldType() {
            return fieldType;
        }

        public String getFieldName() {
            return fieldName;
        }

        public String getMethodNamePrefix() {
            return methodNamePrefix;
        }

        public int getSerializerType() {
            return serializerType;
        }

        public void setSerializerType(int serializerType) {
            this.serializerType = serializerType;
        }

        public void setFieldType(String fieldType) {
            this.fieldType = fieldType;
        }

        public void setFieldName(String fieldName) {
            this.fieldName = fieldName;
        }

        public void setMethodNamePrefix(String methodNamePrefix) {
            this.methodNamePrefix = methodNamePrefix;
        }

        public String getComment() {
            return comment;
        }

        public void setComment(String comment) {
            this.comment = comment;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;
            CSharpExportField that = (CSharpExportField) o;
            return fieldType.equals(that.fieldType) &&
                    fieldName.equals(that.fieldName);
        }

        @Override
        public int hashCode() {
            return Objects.hash(fieldType, fieldName);
        }
    }
}





