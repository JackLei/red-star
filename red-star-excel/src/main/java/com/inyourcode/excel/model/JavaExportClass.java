package com.inyourcode.excel.model;

import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Objects;
import java.util.Set;

/**
 * Java类数据模型
 * @author JackLei
 */
public class JavaExportClass {
    /** 包名 */
    private String packageName;
    /** 类名 */
    private String javaClassName;
    /** 绑定的数据 */
    private String dataFileName;
    /** 所有属性 */
    private Set<JavaExportField> fields = new LinkedHashSet<>();
    /** import集合 */
    private Set<String> imports = new LinkedHashSet<>();
    /** 枚举集合 */
    private List<JavaExportEnum> enumClassList = new ArrayList<>();

    public JavaExportClass(String packageName, String javaClassName) {
        this.packageName = packageName;
        this.javaClassName = "Config" + javaClassName.substring(0 ,1).toUpperCase() + javaClassName.substring(1);
    }

    public String getPackageName() {
        return packageName;
    }

    public String getJavaClassName() {
        return javaClassName;
    }

    public Set<String> getImports() {
        return imports;
    }

    public void setImports(Set<String> imports) {
        this.imports = imports;
    }

    public Set<JavaExportField> getFields() {
        return fields;
    }

    public void setPackageName(String packageName) {
        this.packageName = packageName;
    }

    public void setJavaClassName(String javaClassName) {
        this.javaClassName = javaClassName;
    }

    public void setFields(Set<JavaExportField> fields) {
        this.fields = fields;
    }

    public String getDataFileName() {
        return dataFileName;
    }

    public void setDataFileName(String dataFileName) {
        this.dataFileName = dataFileName;
    }
    
    public List<JavaExportEnum> getEnumClassList() {
        return enumClassList;
    }

    public void setEnumClassList(List<JavaExportEnum> enumClassList) {
        this.enumClassList = enumClassList;
    }

    /**
     * 属性数据模型
     */
    public static class JavaExportField {
        /** 属性类型 */
        private String fieldType;
        /** 属性名字 */
        private String fieldName;
        /** 方法名 */
        private String methodNamePrefix;
        /** 注释 */
        private String comment;
        /* json 序列化类型**/
        private int serializerType;

        public JavaExportField(String fieldType, String fieldName, String comment,int serializerType) {
            this.fieldType = fieldType;
            this.fieldName = fieldName;
            this.comment = comment;
            this.methodNamePrefix = this.fieldName.substring(0, 1).toUpperCase() + this.fieldName.substring(1);
            this.serializerType = serializerType;
        }

        public String getFieldType() {
            return fieldType;
        }

        public String getFieldName() {
            return fieldName;
        }

        public String getMethodNamePrefix() {
            return methodNamePrefix;
        }

        public int getSerializerType() {
            return serializerType;
        }

        public void setSerializerType(int serializerType) {
            this.serializerType = serializerType;
        }

        public void setFieldType(String fieldType) {
            this.fieldType = fieldType;
        }

        public void setFieldName(String fieldName) {
            this.fieldName = fieldName;
        }

        public void setMethodNamePrefix(String methodNamePrefix) {
            this.methodNamePrefix = methodNamePrefix;
        }

        public String getComment() {
            return comment;
        }

        public void setComment(String comment) {
            this.comment = comment;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;
            JavaExportField that = (JavaExportField) o;
            return fieldType.equals(that.fieldType) &&
                    fieldName.equals(that.fieldName);
        }

        @Override
        public int hashCode() {
            return Objects.hash(fieldType, fieldName);
        }
    }

    /**
     * Enum类数据模型
     */
    public static class JavaExportEnum {
        /** 类名*/
        private String enumClassName;
        /** 枚举类型集合 */
        private List<JavaExportEnumElement> fields = new ArrayList<>();

        public String getEnumClassName() {
            return enumClassName;
        }

        public void setEnumClassName(String enumClassName) {
            this.enumClassName = enumClassName;
        }

        public List<JavaExportEnumElement> getFields() {
            return fields;
        }

        public void setFields(List<JavaExportEnumElement> fields) {
            this.fields = fields;
        }
    }

    /**
     * 枚举类型数据模型
     */
    public static class JavaExportEnumElement {
        /** 类型 */
        private int type;
        /** 描述 */
        private String desc;
        /** 注释 */
        private String comment;

        public int getType() {
            return type;
        }

        public void setType(int type) {
            this.type = type;
        }

        public String getDesc() {
            return desc;
        }

        public void setDesc(String desc) {
            this.desc = desc;
        }

        public String getComment() {
            return comment;
        }

        public void setComment(String comment) {
            this.comment = comment;
        }
    }

}





