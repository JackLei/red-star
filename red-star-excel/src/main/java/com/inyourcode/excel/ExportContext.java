/*
 * Copyright (c) 2022 The red-star Project
 *
 * Licensed under the Apache License, version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at:
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.inyourcode.excel;

import com.inyourcode.excel.model.SheetDataModel;

import java.util.Map;
import java.util.Properties;

/**
 * 导出数据时，需要的上下文
 * @author JackLei
 */
public class ExportContext {
    /** 配置文件 */
    private Properties properties;
    /** excel数据模型 */
    private Map<String, SheetDataModel> sheetDataModelMap;

    public static ExportContext create(Properties properties, Map<String, SheetDataModel> dataModel) {
        ExportContext dto = new ExportContext();
        dto.properties = properties;
        dto.sheetDataModelMap = dataModel;
        return dto;
    }

    public Properties getProperties() {
        return properties;
    }

    public Map<String, SheetDataModel> getSheetDataModelMap() {
        return sheetDataModelMap;
    }
}
