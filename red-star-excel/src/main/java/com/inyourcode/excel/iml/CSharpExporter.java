package com.inyourcode.excel.iml;

import com.google.common.base.Strings;
import com.google.common.io.Resources;
import com.inyourcode.excel.ExportContext;
import com.inyourcode.excel.ExportException;
import com.inyourcode.excel.api.IExporter;
import com.inyourcode.excel.model.CSharpExportClass;
import com.inyourcode.excel.model.SheetDataModel;
import com.inyourcode.excel.model.column.CommentColData;
import com.inyourcode.excel.model.column.NameColData;
import com.inyourcode.excel.model.column.TypeColData;
import freemarker.cache.StringTemplateLoader;
import freemarker.template.Configuration;
import freemarker.template.Template;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.nio.charset.Charset;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * java类导出器
 * @author JackLei
 */
public class CSharpExporter implements IExporter {
    private static final Logger LOGGER = LoggerFactory.getLogger(CSharpExporter.class);
    /** 配置属性的key: java类包名 */
    private static final String KEY_PACKAGE_PATH = "cs.out.package";
    /** 配置属性的key: java类导出路径   */
    private static final String KEY_CS_OUT_PATH = "cs.out.path";
    /** 配置属性的key: java类导出模版 */
    private static final String FILE_TEMPLATE_NAME = "cs-export.ftl";

    @Override
    public boolean check(ExportContext context) throws ExportException {
        String outPath = context.getProperties().getProperty(KEY_CS_OUT_PATH);
        if (Strings.isNullOrEmpty(outPath)) {
            throw new ExportException("Please set cs export path");
        }
        return true;
    }

    @Override
    public void export(ExportContext context) throws ExportException {

        Properties properties = context.getProperties();
        String packageName = properties.getProperty(KEY_PACKAGE_PATH);
        String outPath = properties.getProperty(KEY_CS_OUT_PATH);

        //初始化导出模版
        Map<String, SheetDataModel> sheetDataModelMap = context.getSheetDataModelMap();
        Template template;
        try {
            String templateString = Resources.toString(this.getClass().getClassLoader().getResource(FILE_TEMPLATE_NAME), Charset.forName("UTF-8"));
            Configuration cfg = new Configuration(Configuration.DEFAULT_INCOMPATIBLE_IMPROVEMENTS);
            StringTemplateLoader loader = new StringTemplateLoader();
            loader.putTemplate(FILE_TEMPLATE_NAME, templateString);
            cfg.setTemplateLoader(loader);
            cfg.setDefaultEncoding("UTF-8");

            template = cfg.getTemplate(FILE_TEMPLATE_NAME);
        } catch (IOException e) {
            LOGGER.error("init template configuration error, template file name:{}", FILE_TEMPLATE_NAME);
            throw new ExportException("init template configuration error", e);
        }

        for (Map.Entry<String, SheetDataModel> entry : sheetDataModelMap.entrySet()) {
            String name = entry.getKey();
            SheetDataModel model = entry.getValue();

            //初始化导出的java类的数据模型
            String javaClassName = name.substring(0,1).toUpperCase() + name.substring(1);
            CSharpExportClass exportClass = new CSharpExportClass(packageName, javaClassName);

            //初始化导出的java类的属性
            CommentColData[] commentHeader = model.getCommentHeader();
            NameColData[] nameHeader = model.getNameHeader();
            TypeColData[] typeHeader = model.getTypeHeader();
            try {
                for (int index = 0; index < nameHeader.length; index++) {
                    if (commentHeader[index].isIgnore())
                    {
                        continue;
                    }
                    String fieldName = commentHeader[index].getVal().toString();
                    String type = "";
                    /**指定json序列化器*/
                    int serializeType = 0;
                    if (typeHeader[index].isFloat()) {
                        type = "float";
                    } else if (typeHeader[index].isInt()) {
                        type = "int";
                    } else if (typeHeader[index].isString()) {
                        type = "string";
                    }  else {
                        LOGGER.error("export cs error, type not found, type:{}, data:{},name:{}", typeHeader[index].getVal(), typeHeader[index], name);
                        continue;
                    }

                    CSharpExportClass.CSharpExportField exportField = new CSharpExportClass.CSharpExportField(type, nameHeader[index].getVal().toString(), fieldName, serializeType);
                    exportClass.getFields().add(exportField);
                }

                String genPackageName = exportClass.getPackageName();
                String genClassName = exportClass.getJavaClassName() + ".cs";
                String parentPath = outPath.concat(Stream.of(genPackageName.split("\\."))
                        .collect(Collectors.joining("/", "/", "/")));

                //目录不存在，先创建
                File parentFile = new File(parentPath);
                if (!parentFile.exists()) {
                    parentFile.mkdirs();
                }

                //创建java文件
                String fileOutPath = parentPath + File.separator + genClassName;
                File fileOut = new File(fileOutPath);
                if (!fileOut.exists()) {
                    fileOut.createNewFile();
                }

                LOGGER.info("export cs class,path = {}", fileOutPath);

                OutputStreamWriter outputStreamWriter = new OutputStreamWriter(new FileOutputStream(fileOut));
                template.process(exportClass, outputStreamWriter);
            }catch (Exception ex) {
                LOGGER.error("sheetName:{}, export cs failed", name);
                throw new ExportException("export cs error", ex);
            }

        }
    }

}
