/***
 * <pre>
 *     此类由工具自动生成的代码,不可以修改.
 * </pre>
 * @author JackLei
 */
public partial class ${javaClassName} {

<#list  fields as field>
    /** ${field.comment} */
    public ${field.fieldType}  ${field.fieldName};
</#list>

}
