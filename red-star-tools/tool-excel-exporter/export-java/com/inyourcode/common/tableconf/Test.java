package com.inyourcode.common.tableconf;

import com.alibaba.fastjson.JSONArray;
import com.inyourcode.excel.serializer.JavaExcelEnum;
import com.inyourcode.excel.api.ExcelTable;
import com.inyourcode.excel.serializer.JavaListSerializer;
import com.inyourcode.excel.serializer.JavaExcelList;
import com.alibaba.fastjson.annotation.JSONField;
import com.inyourcode.excel.serializer.JavaEnumSerializer;

/***
 * <pre>
 *     此类由工具自动生成的代码,不可以修改.
 * </pre>
 * @author JackLei
 */
@ExcelTable(bindData = "Test.json")
public class Test {

    /** 唯一Id */
    private int  id;
    /** 整形 */
    private int  subId;
    /** 浮点型 */
    private float  p1;
    /** 字符串 */
    private String  str;
    /** 枚举  */
    @JSONField(deserializeUsing = JavaEnumSerializer.class)
    private TestEnumEnums  enums;
    /** 字符串数组 */
    @JSONField(deserializeUsing = JavaListSerializer.class)
    private JavaExcelList<String>  strings;
    /** 浮点数数组 */
    @JSONField(deserializeUsing = JavaListSerializer.class)
    private JavaExcelList<Float>  floats;
    /** 属性范围 */
    @JSONField(deserializeUsing = JavaListSerializer.class)
    private JavaExcelList<int[]>  arrys;
    /** 属性权重 */
    @JSONField(deserializeUsing = JavaListSerializer.class)
    private JavaExcelList<Integer>  arry;


    public void setId(int id){
        this.id = id;
    }

    public int getId(){
        return this.id;
    }

    public void setSubId(int subId){
        this.subId = subId;
    }

    public int getSubId(){
        return this.subId;
    }

    public void setP1(float p1){
        this.p1 = p1;
    }

    public float getP1(){
        return this.p1;
    }

    public void setStr(String str){
        this.str = str;
    }

    public String getStr(){
        return this.str;
    }

    public void setEnums(TestEnumEnums enums){
        this.enums = enums;
    }

    public TestEnumEnums getEnums(){
        return this.enums;
    }

    public void setStrings(JavaExcelList<String> strings){
        this.strings = strings;
    }

    public JavaExcelList<String> getStrings(){
        return this.strings;
    }

    public void setFloats(JavaExcelList<Float> floats){
        this.floats = floats;
    }

    public JavaExcelList<Float> getFloats(){
        return this.floats;
    }

    public void setArrys(JavaExcelList<int[]> arrys){
        this.arrys = arrys;
    }

    public JavaExcelList<int[]> getArrys(){
        return this.arrys;
    }

    public void setArry(JavaExcelList<Integer> arry){
        this.arry = arry;
    }

    public JavaExcelList<Integer> getArry(){
        return this.arry;
    }

    public static enum TestEnumEnums implements JavaExcelEnum {
        PassMission(1,"PassMission"),//通关副本
        Finish(2,"Finish"),//完成任务

        ;

        private int type;
        private String desc;

        TestEnumEnums(int type, String desc) {
            this.type = type;
            this.desc = desc;
        }

        @Override
        public int type() {
            return type;
        }

        @Override
        public String desc() {
            return desc;
        }

        public  static TestEnumEnums getEnumType(int type) {
            for(TestEnumEnums element  : values()) {
                if (element.type == type) {
                    return element;
                }
            }

            return null;
        }
    }


}