#!\bin\bash
#.protobuf源文件的绝对路径
protoSrc=$1
#生成java文件的绝对路径
javaOutFile=$2

for file in `ls $protoSrc`
do
    if echo "$file" | grep -q -E '.proto$'
    then
    echo "Proto source file: "$protoSrc"/"$file
    protoc --java_out=$javaOutFile $protoSrc"/"$file --proto_path=$protoSrc
    echo $file".java generated successfully"
    fi
done
