@echo off
echo ** setting runtime variable
set protoSrc=%0
set java_out_file=%1

for /R "%protoSrc%" %%i in (*) do (
	set filename=%%~nxi
	if "%%~xi"  == ".proto" (
		protoc.exe --proto_path=%protoSrc% --java_out=%java_out_file% %%i
	    echo %java_out_file% %%i success
	)
)
pause