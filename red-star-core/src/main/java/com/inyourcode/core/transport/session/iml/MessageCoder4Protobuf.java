/*
 * Copyright (c) 2021 The red-star Project
 *
 * Licensed under the Apache License, version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at:
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.inyourcode.core.transport.session.iml;

import com.google.protobuf.AbstractMessage;
import com.inyourcode.core.serialization.api.Serializer;
import com.inyourcode.core.serialization.api.SerializerFactory;
import com.inyourcode.core.serialization.api.SerializerType;
import com.inyourcode.core.transport.api.payload.BytesHolder;
import com.inyourcode.core.transport.api.payload.JRequestBytes;
import com.inyourcode.core.transport.api.payload.JResponseBytes;
import com.inyourcode.core.transport.session.HandlerWrapper;
import com.inyourcode.core.transport.session.ResponseContext;
import com.inyourcode.core.transport.session.RequestContext;
import com.inyourcode.core.transport.session.SessionHandlerMapping;
import com.inyourcode.core.transport.session.api.IMessageCoder;
import com.inyourcode.core.transport.session.api.MessageHolder;
import com.inyourcode.core.transport.session.head.ProtocolHeadProto;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author JackLei
 */
public class MessageCoder4Protobuf implements IMessageCoder {
    private static final Logger LOGGER = LoggerFactory.getLogger(MessageCoder4Protobuf.class);

    @Override
    public byte serializerType() {
        return SerializerType.PROTOBUF.value();
    }

    @Override
    public BytesHolder encode(MessageHolder messageHolder) {
        int traceId = messageHolder.getTraceId();
        long invokeId = messageHolder.getInvokeId();
        Object message = messageHolder.getData();
        ProtocolHeadProto.ProtobufHead.Builder headBuilder = ProtocolHeadProto.ProtobufHead.newBuilder();
        headBuilder.setTraceId(traceId);
        if (!(message instanceof AbstractMessage.Builder)) {
            return null;
        }

        AbstractMessage.Builder builder = (AbstractMessage.Builder) message;
        headBuilder.setMessage(builder.build().toByteString());
        headBuilder.setTimestamp(messageHolder.getTimeStamp());

        Serializer serializer = SerializerFactory.getSerializer(SerializerType.PROTOBUF.value());

        if (messageHolder instanceof RequestContext) {
            JRequestBytes jRequestPayload = new JRequestBytes(invokeId);
            jRequestPayload.bytes(SerializerType.PROTOBUF.value(), serializer.writeObject(headBuilder));
            return jRequestPayload;
        } else if (messageHolder instanceof ResponseContext) {
            JResponseBytes jResponseBytes = new JResponseBytes(invokeId);
            jResponseBytes.bytes(SerializerType.PROTOBUF.value(), serializer.writeObject(headBuilder));
            return jResponseBytes;
        } else {
            return null;
        }


    }

    @Override
    public MessageHolder decode(BytesHolder bytesHolder) {
        byte serializerCode = bytesHolder.serializerCode();
        long invokeId = bytesHolder.invokeId();
        Serializer serializer = SerializerFactory.getSerializer(serializerCode);
        byte[] bytes = bytesHolder.bytes();

        if (serializer == null) {
            LOGGER.error("The serializer could not be found when type:{}", serializerCode);
            return null;
        }

        HandlerWrapper wrapper = SessionHandlerMapping.getHandlerWrapper(invokeId);
        if (wrapper == null) {
            LOGGER.error("The wrapper could not be found when invokeId: {}", invokeId);
            return null;
        }

        ProtocolHeadProto.ProtobufHead head = serializer.readObject(bytes, ProtocolHeadProto.ProtobufHead.class);
        if (head == null) {
            LOGGER.error("The protocol head is null when invokeId: {}", invokeId);
            return null;
        }

        Object message = serializer.readObject(head.getMessage().toByteArray(), wrapper.getMessageClazz());
        if (bytesHolder instanceof JRequestBytes) {
            RequestContext requestContext = new RequestContext(invokeId, head.getTraceId(), message, serializerCode, head.getTimestamp());
            return requestContext;
        } else  if (bytesHolder instanceof JResponseBytes) {
            ResponseContext responseContext = new ResponseContext(invokeId, head.getTraceId(), message, serializerCode, head.getTimestamp());
            return responseContext;
        } else {
            return null;
        }

    }

}
