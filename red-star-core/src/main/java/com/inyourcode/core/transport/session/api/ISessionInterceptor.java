package com.inyourcode.core.transport.session.api;


import com.inyourcode.core.transport.session.HandlerWrapper;

public interface ISessionInterceptor {
    ISessionInterceptor DEFALUT = new ISessionInterceptor() {
        @Override
        public boolean intercept(HandlerWrapper processerWrapper, Object message, Session session) {
            return true;
        }
    };

    boolean intercept(HandlerWrapper processerWrapper, Object message, Session session);
}
