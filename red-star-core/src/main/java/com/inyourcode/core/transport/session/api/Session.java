/*
 * Copyright (c) 2021 The red-star Project
 *
 * Licensed under the Apache License, version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at:
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.inyourcode.core.transport.session.api;


import com.inyourcode.core.db.api.IDataContext;
import com.inyourcode.core.threads.ConsumerTask;
import com.inyourcode.core.threads.api.HashExecutor;
import io.netty.channel.Channel;
import io.netty.channel.ChannelFutureListener;

import java.util.concurrent.Executor;

/**
 *
 * @author JackLei
 */
public interface Session extends Executor {
     /** session的唯一id  */
     Long getId();

     void setId(long id);

    HashExecutor getExecutor();

     void setExecutor(HashExecutor executor);

     void execute(Runnable runnable);

     /** channel */
     Channel channel();

     void setChannel(Channel channel);

     /** 是否可用 */
     boolean isActive();

    void write(MessageHolder messageHolder);

    void write(MessageHolder messageHolder, ChannelFutureListener listener);

    void closeAndNotify(MessageHolder messageHolder);

    /** 刷新缓冲区 */
    void flush();

    /** 是否验证通过 */
    boolean isAuth();

    void setAuth(boolean auth);

    void replaceInfo(Session session);

    void loadAllData(HashExecutor dbExecutor, ConsumerTask callBackTask);

    void saveAllData(HashExecutor dbExecutor, ConsumerTask callBackTask);

    void setDataContext(IDataContext dataContext);

    <T> T getContextData(Class<T> clazz);

    <T> T getContextDataWhenNullCreate(Class<T> clazz);

    void putContextData(Object data);

    void setAttribute(String key, Object val);

    Object getAttribute(String key);

    void cacheDataOptExpire(boolean clear, int expireTime);

    int state();
}
