/*
 * Copyright (c) 2015 The Jupiter Project
 *
 * Licensed under the Apache License, version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at:
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.inyourcode.core.transport.rpc.provider.processor;
import com.inyourcode.core.threads.api.ExecutorFactory;
import com.inyourcode.core.threads.ExecutorFactoryManager;
import com.inyourcode.core.threads.api.CloseableExecutor;
import com.inyourcode.core.transport.rpc.provider.processor.task.ProviderTask;
import com.inyourcode.core.transport.api.Directory;
import com.inyourcode.core.transport.api.channel.JChannel;
import com.inyourcode.core.transport.api.payload.JRequestBytes;
import com.inyourcode.core.transport.rpc.JRequest;
import com.inyourcode.core.transport.rpc.JServer;
import com.inyourcode.core.transport.rpc.control.ControlResult;
import com.inyourcode.core.transport.rpc.control.FlowController;
import com.inyourcode.core.transport.rpc.metadata.ServiceWrapper;

/**
 * jupiter
 * provider.processor
 *
 * @author jiachun.fjc
 */
public class RpcProviderProcessor extends AbstractProviderProcessor {

    private final JServer server;
    private final CloseableExecutor executor;

    public RpcProviderProcessor(JServer server) {
        this(server, ExecutorFactoryManager.newExecutor(ExecutorFactory.Target.PROVIDER));
    }

    public RpcProviderProcessor(JServer server, CloseableExecutor executor) {
        this.server = server;
        this.executor = executor;
    }

    @Override
    public void handleRequest(JChannel channel, JRequestBytes requestBytes) throws Exception {
        ProviderTask task = new ProviderTask(this, channel, new JRequest(requestBytes));
        if (executor == null) {
            task.run();
        } else {
            executor.execute(task);
        }
    }

    @Override
    public ServiceWrapper lookupService(Directory directory) {
        return server.lookupService(directory);
    }

    @Override
    public ControlResult flowControl(JRequest request) {
        // 全局流量控制
        FlowController<JRequest> controller = server.globalFlowController();
        if (controller == null) {
            return ControlResult.ALLOWED;
        }
        return controller.flowControl(request);
    }
}
