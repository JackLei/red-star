/*
 * Copyright (c) 2021 The red-star Project
 *
 * Licensed under the Apache License, version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at:
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.inyourcode.core.transport.session.api;

/**
 * @author JackLei
 */
public class MessageHolder {
    private long invokeId;
    private int  traceId;
    private Object data;
    private byte serializerCode;
    private long timeStamp;

    public MessageHolder() {
        super();
    }

    public MessageHolder(long invokeId, int traceId, Object data) {
        this.invokeId = invokeId;
        this.traceId = traceId;
        this.data = data;
    }

    public MessageHolder(long invokeId,  Object data) {
        this.invokeId = invokeId;
        this.data = data;
    }

    public MessageHolder(long invokeId, int traceId, Object data, byte serializerCode) {
        this.invokeId = invokeId;
        this.traceId = traceId;
        this.data = data;
        this.serializerCode = serializerCode;
    }

    public MessageHolder(long invokeId, int traceId, Object data, byte serializerCode, long timeStamp) {
        this.invokeId = invokeId;
        this.traceId = traceId;
        this.data = data;
        this.serializerCode = serializerCode;
        this.timeStamp = timeStamp;
    }

    public long getInvokeId() {
        return invokeId;
    }

    public <T> T getData() {
        return (T) data;
    }

    public int getTraceId() {
        return traceId;
    }

    public byte getSerializerCode() {
        return serializerCode;
    }

    public void setInvokeId(long invokeId) {
        this.invokeId = invokeId;
    }

    public void setTraceId(int traceId) {
        this.traceId = traceId;
    }

    public void setData(Object data) {
        this.data = data;
    }

    public void setSerializerCode(byte serializerCode) {
        this.serializerCode = serializerCode;
    }

    public long getTimeStamp() {
        return timeStamp;
    }

    public void setTimeStamp(long timeStamp) {
        this.timeStamp = timeStamp;
    }
}
