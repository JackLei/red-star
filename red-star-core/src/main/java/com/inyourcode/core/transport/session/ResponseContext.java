/*
 * Copyright (c) 2021 The red-star Project
 *
 * Licensed under the Apache License, version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at:
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.inyourcode.core.transport.session;

import com.inyourcode.core.transport.session.api.MessageHolder;

/**
 * @author JackLei
 */
public class ResponseContext extends MessageHolder {

    public ResponseContext(long invokeId, int traceId, Object data, byte serializerCode) {
        super(invokeId, traceId, data, serializerCode);
    }

    public ResponseContext(long invokeId, int traceId, Object data, byte serializerCode, long timeStamp) {
        super(invokeId, traceId, data, serializerCode, timeStamp);
    }

    public ResponseContext(long invokeId, Object data) {
        super(invokeId, data);
    }

    public static ResponseContext create(long invokeId, Object data) {
        return new ResponseContext(invokeId,  data);
    }

    public static ResponseContext create(long invokeId, int traceId, Object data, byte serializerCode) {
        return new ResponseContext(invokeId, traceId, data, serializerCode);
    }

}
