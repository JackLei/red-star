/*
 * Copyright (c) 2021 The red-star Project
 *
 * Licensed under the Apache License, version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at:
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.inyourcode.core.transport.session.api;

import com.inyourcode.core.transport.api.payload.BytesHolder;

/**
 * 对网络层传下来的数据，应用层定义了消息头，需要自己包装请求/返回消息，然后放在消息头里。其他序列化类型，可以在这里实现
 * @author JackLei
 */
public interface IMessageCoder {

    byte serializerType();

    /**
     *
     * @param messageHolder
     * @return
     */
    BytesHolder encode(MessageHolder messageHolder);

    MessageHolder decode(BytesHolder requestPayload);
}
