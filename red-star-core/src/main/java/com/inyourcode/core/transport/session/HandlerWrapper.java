/*
 * Copyright (c) 2021 The red-star Project
 *
 * Licensed under the Apache License, version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at:
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.inyourcode.core.transport.session;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

/**
 * @author JackLei
 */
public class HandlerWrapper {
    private long invokerId;
    private Class messageClazz;
    private Method method;
    private Object processor;
    private boolean asyncReq;
    private boolean fixedReq;
    private boolean noAuthReq;

    public HandlerWrapper(long invokerId, Class messageClazz, Method method, Object processor, boolean asyncReq, boolean fixed, boolean noAuthReq) {
        this.invokerId = invokerId;
        this.messageClazz = messageClazz;
        this.method = method;
        this.processor = processor;
        this.asyncReq = asyncReq;
        this.fixedReq = fixed;
        this.noAuthReq = noAuthReq;
    }

    public long getInvokerId() {
        return invokerId;
    }

    public Class getMessageClazz() {
        return messageClazz;
    }

    public Object getProcessor() {
        return processor;
    }

    public Method getMethod() {
        return method;
    }

    public boolean isAsyncReq() {
        return asyncReq;
    }

    public boolean isFixedReq() {
        return fixedReq;
    }

    public boolean isNoAuthReq() {
        return noAuthReq;
    }

    public Object invoke(Object... param) throws InvocationTargetException, IllegalAccessException {
        return this.method.invoke(this.processor, param);
    }


    @Override
    public String toString() {
        return "HandlerWrapper{" +
                "invokerId=" + invokerId +
                ", clazz=" + messageClazz.getSimpleName() +
                ", method=" + method.getName() +
                '}';
    }
}
