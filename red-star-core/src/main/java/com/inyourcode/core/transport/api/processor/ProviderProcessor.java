/*
 * Copyright (c) 2015 The Jupiter Project
 *
 * Licensed under the Apache License, version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at:
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.inyourcode.core.transport.api.processor;


import com.inyourcode.core.transport.api.Status;
import com.inyourcode.core.transport.api.channel.JChannel;
import com.inyourcode.core.transport.api.payload.JRequestBytes;
import io.netty.channel.ChannelHandlerContext;

/**
 * Provider's processor.
 *
 * jupiter 的这个抽象很接地气，不同的业务处理请求的方式不一样，这一层要抽象出来。
 * 只要涉及到任务的处理，就是生产者、消费者模型，所以要抽象有两个必要的参数：
 * 1、数据（网络层传来的消息），Jupiter已经抽象为JRequestBytes
 * 2、线程（Executor），消费任务
 *
 * 处理方式已经抽象出来了，可变的地方, 应该只有用不同的Executor，这个从抽象简直完美
 *
 * @author jiachun.fjc
 * @author JackLei
 */
public interface ProviderProcessor<T> {

    /**
     * 处理正常请求
     */
    void handleRequest(JChannel channel, T request) throws Exception;

    /**
     * 处理异常
     */
    void handleException(JChannel channel, T request, Status status, Throwable cause);


    default void channelActive(ChannelHandlerContext ctx) throws Exception {

    }

    default void channelInactive(ChannelHandlerContext ctx) throws Exception {

    }
}
