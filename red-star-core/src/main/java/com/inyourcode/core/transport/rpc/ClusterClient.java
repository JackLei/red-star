/*
 * Copyright (c) 2015 The Jupiter Project
 *
 * Licensed under the Apache License, version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at:
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.inyourcode.core.transport.rpc;

import com.inyourcode.core.util.ClassInitializeUtil;
import com.inyourcode.core.GlobalConstants;
import com.inyourcode.core.transport.rpc.consumer.processor.ClusterNodeConsumerProcessor;
import com.inyourcode.core.transport.api.JConnection;
import com.inyourcode.core.transport.api.JConnector;

import static com.inyourcode.core.util.Preconditions.checkNotNull;


/**
 * 集群客户端实现.
 *
 * jupiter
 * org.jupiter.rpc
 *
 * @author jiachun.fjc
 * @author  Jacklei
 */
public class ClusterClient implements JClient {

    static {
        // touch off TracingUtil.<clinit>
        // because getLocalAddress() and getPid() sometimes too slow
        ClassInitializeUtil.initClass("com.inyourcode.core.transport.rpc.tracing.TracingUtil", 500);
    }

    // 服务订阅(SPI)
    private final String appName;

    protected JConnector<JConnection> connector;

    public ClusterClient() {
        this(GlobalConstants.UNKNOWN_APP_NAME);
    }

    public ClusterClient(String appName) {
        this.appName = appName;
    }

    @Override
    public String appName() {
        return appName;
    }

    @Override
    public JConnector<JConnection> connector() {
        return connector;
    }

    @Override
    public JClient withConnector(JConnector<JConnection> connector) {
        connector.withProcessor(new ClusterNodeConsumerProcessor());
        this.connector = connector;
        return this;
    }

    @Override
    public void shutdownGracefully() {
        connector.shutdownGracefully();
    }

    // for spring-support
    public void setConnector(JConnector<JConnection> connector) {
        withConnector(connector);
    }

}
