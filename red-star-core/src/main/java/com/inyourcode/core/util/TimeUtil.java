/*
 * Copyright (c) 2021 The red-star Project
 *
 * Licensed under the Apache License, version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at:
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.inyourcode.core.util;

/**
 * @author JackLei
 */
public class TimeUtil {
    public static final String C_TIME_PATTON_DEFAULT = "yyyy-MM-dd HH:mm:ss";
    public static final String C_DATE_PATTON_DEFAULT = "yyyy-MM-dd";
    public static final String C_DATA_PATTON_YYYYMMDD = "yyyyMMdd";
    public static final String C_TIME_PATTON_HHMMSS = "HH:mm:ss";

    /** 每天多少小时 */
    private static final int HOUR_PER_DAY = 24;
    /** 每分钟多少秒 */
    public static final int SECOND_PER_MINUTE = 1 * 60;
    /** 每小時多少秒 */
    public static final int SECOND_PER_HOUR = SECOND_PER_MINUTE * 60;
    /** 每天多少秒 */
    public static final int SECOND_PER_DAY = SECOND_PER_HOUR * 24;

    /** 每秒多少毫秒 */
    public static final int MILLIS_PER_SECOND = 1000;
    /** 每分钟多少毫秒 */
    public static final int MILLIS_PER_MINUTE = MILLIS_PER_SECOND * SECOND_PER_MINUTE;
    /** 每小时多少毫秒 */
    public static final int MILLIS_PER_HOUR = MILLIS_PER_SECOND * SECOND_PER_HOUR;
    /** 每天多少毫秒 */
    public static final long MILLIS_PER_DAY = MILLIS_PER_SECOND * SECOND_PER_DAY;

    /***/
    private static long OFFSET_TIME_SEC = 0;

    public void setOffsetTimeSec(int offsetTimeSec) {
        OFFSET_TIME_SEC = offsetTimeSec;
    }

    public static long currentTimeMillis() {
        return System.currentTimeMillis() + OFFSET_TIME_SEC;
    }


}
