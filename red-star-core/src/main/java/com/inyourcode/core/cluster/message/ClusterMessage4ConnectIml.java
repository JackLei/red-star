package com.inyourcode.core.cluster.message;

import com.inyourcode.core.cluster.ClusterNodeConf;
import com.inyourcode.core.transport.rpc.ServiceProviderImpl;

@ServiceProviderImpl
public class ClusterMessage4ConnectIml implements ClusterMessage4Connect{
    public void clusterNodeActive(ClusterNodeConf clusterNodeConf) {
        System.out.println(clusterNodeConf);
    }
}
