package com.inyourcode.core.cluster.message;

import com.inyourcode.core.cluster.ClusterNodeConf;
import com.inyourcode.core.GlobalConstants;
import com.inyourcode.core.transport.rpc.ServiceProvider;

@ServiceProvider(group = GlobalConstants.GROUP_CLUSTER)
public interface ClusterMessage4Connect {
    public void clusterNodeActive(ClusterNodeConf clusterNodeConf) ;
}

