/*
 * Copyright (c) 2021 The red-star Project
 *
 * Licensed under the Apache License, version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at:
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.inyourcode.core.cluster;

import com.inyourcode.core.cluster.api.IClusterNodeType;

/**
 * @author JackLei
 */
public enum ClusterType implements IClusterNodeType {
    LOBBY("lobby"),
    MATCH("match"),
    CHAT("chat"),
    WEB("web"),
    LOGIN("login"),
    FIGHT("fight"),
    COMMON("common"),
    MAIL("mail"),
    FRIEND("friend")




    ;

    private String type;

    ClusterType(String type) {
        this.type = type;
    }

    public static ClusterType getType(String type) {
        for (ClusterType clusterType : values()) {
            if (clusterType.type.equals(type)) {
                return  clusterType;
            }
        }

        return null;
    }

    @Override
    public String getType() {
        return type;
    }
}
