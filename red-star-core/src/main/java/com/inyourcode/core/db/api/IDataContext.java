package com.inyourcode.core.db.api;

import com.inyourcode.core.threads.ConsumerTask;
import com.inyourcode.core.threads.api.HashExecutor;
import com.inyourcode.core.transport.session.api.Session;

/**
 * @author JackLei
 */
public interface IDataContext {
    Long id();

    void loadAll();

    void saveAll(HashExecutor dbExecutor, Session session, ConsumerTask callBack);

    void updateData(Object data);

    <T> T getData(Class<T> type);

    void putData(Object data);

    String dataKey();

    int state();

    void cacheDataOptExpire(boolean clear, int expireTime);
}
