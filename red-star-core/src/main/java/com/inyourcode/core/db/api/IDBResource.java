package com.inyourcode.core.db.api;

import java.util.Map;

/**
 * @author JackLei
 */
public interface IDBResource {
    Map<String ,byte[]> load(String key);

    void save(String key, Map<String ,byte[]> update);



}
