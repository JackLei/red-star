package com.inyourcode.core.db;

import com.inyourcode.core.db.api.Entity;
import com.inyourcode.core.spring.ClassHelper;
import java.util.HashSet;
import java.util.Set;

/**
 * @author JackLei
 */
public class DataClassFilter implements ClassHelper.IClassScannerFilter {
    private Set<String> keySet = new HashSet<>();

    @Override
    public boolean filter(Class clazz) throws ClassHelper.ClassScannerException {
        if (!clazz.isAnnotationPresent(Entity.class)) {
            return false;
        }

        Entity annotation = (Entity) clazz.getAnnotation(Entity.class);
        String key = annotation.key();

        if (keySet.contains(key)) {
            throw new ClassHelper.ClassScannerException(String.format("Filter exception due to duplicate naming, class:%s, key:%s", clazz, key));
        }

        keySet.add(key);
        return true;
    }
}
