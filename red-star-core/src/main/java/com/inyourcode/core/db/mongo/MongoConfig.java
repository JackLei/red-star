/*
 * Copyright (c) 2021 The red-star Project
 *
 * Licensed under the Apache License, version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at:
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.inyourcode.core.db.mongo;

import com.mongodb.MongoClient;
import com.mongodb.MongoClientURI;
import org.springframework.beans.factory.annotation.Configurable;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Conditional;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.data.mongodb.core.MongoTemplate;

/**
 * @author JackLei
 * <a>https://docs.spring.io/spring-data/mongodb/docs/3.3.0-M2/reference/html/#introduction</a>
 */
@Configurable
@Configuration
@PropertySource("classpath:mongo.properties")
public class MongoConfig {
    @Value("${connectionString}")
    private String connectionString;
    private MongoClientURI url;

    @Bean
    @Conditional(MongoCondition.class)
    public MongoClient mongoClient() {
        this.url = new MongoClientURI(connectionString);
        return new MongoClient(this.url);
    }

    @Bean
    @Conditional(MongoCondition.class)
    public MongoTemplate mongoTemplate(MongoClient mongoClient) {
        return new MongoTemplate(mongoClient, this.url.getDatabase());
    }
}
