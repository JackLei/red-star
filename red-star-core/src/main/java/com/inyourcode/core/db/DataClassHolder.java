package com.inyourcode.core.db;

import com.inyourcode.core.db.api.Entity;
import com.inyourcode.core.db.api.IDataClassHolder;

import java.util.ArrayList;
import java.util.List;

/**
 * @author JackLei
 */
public class DataClassHolder implements IDataClassHolder {
    private List<Class> classList;
    private List<EntityWrapper> entityWrappers = new ArrayList<>();

    public DataClassHolder(List<Class> classList) {
        this.classList = classList;
        this.classList.forEach(clazz->{
            if (clazz.isAnnotationPresent(Entity.class)) {
                Entity annotation = (Entity) clazz.getAnnotation(Entity.class);
                String key = annotation.key();
                EntityWrapper wrapper = new EntityWrapper();
                wrapper.setClazz(clazz);
                wrapper.setKey(key);
                this.entityWrappers.add(wrapper);
            }
        });
    }

    @Override
    public List<EntityWrapper> wrap() {
        return this.entityWrappers;
    }
}
