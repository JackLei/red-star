package com.inyourcode.core.db;

/**
 * @author JackLei
 */
public class EntityWrapper {
    private String key;
    private Class clazz;

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public Class getClazz() {
        return clazz;
    }

    public void setClazz(Class clazz) {
        this.clazz = clazz;
    }
}
