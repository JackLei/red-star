package com.inyourcode.core.db.api;

import com.inyourcode.core.db.EntityWrapper;

import java.util.List;

/**
 * @author JackLei
 */
public interface IDataClassHolder {
    List<EntityWrapper> wrap();
}
