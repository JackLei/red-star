package com.inyourcode.core.event;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

/**
 * @author JackLei
 */
public class EventProxy implements Comparable<EventProxy>{
    private String name;
    private Method method;
    private Object object;
    private int priority;

    public EventProxy(String name, Method method, Object object, int priority) {
        this.name = name;
        this.method = method;
        this.object = object;
        this.priority = priority;
    }

    public String getName() {
        return name;
    }

    public void invoke(Object arg) throws InvocationTargetException, IllegalAccessException {
         method.invoke(object, arg);
    }

    @Override
    public int compareTo(EventProxy eventProxy) {
        return this.priority > eventProxy.priority ? 1 : this.priority < eventProxy.priority ? -1 : 0;
    }
}
