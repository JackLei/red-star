/*
 * Copyright (c) 2022 The red-star Project
 *
 * Licensed under the Apache License, version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at:
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.inyourcode.core.monitor.iml;

import com.inyourcode.core.monitor.api.AbstractMonitorOpt;
import io.netty.channel.Channel;
import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.Option;

/**
 * @author JackLei
 */
public class MonitorOpt4NodeName extends AbstractMonitorOpt {

    @Override
    public String opt() {
        return "nodeName";
    }

    @Override
    public void initOptions() {
        Option option = new Option(opt(), true, description());
        options.addOption(option);
    }

    @Override
    public String description() {
        return "节点名，与node选项搭配使用";
    }

    @Override
    public void process(Channel channel, CommandLine commandLine) {

    }

}
