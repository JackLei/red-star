/*
 * Copyright (c) 2022 The red-star Project
 *
 * Licensed under the Apache License, version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at:
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.inyourcode.core.monitor.api;


import io.netty.channel.Channel;
import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.DefaultParser;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;

import java.util.Objects;

/**
 * @author JackLei
 */
public abstract class AbstractMonitorOpt {
    protected static Options options = new Options();
    private static CommandLineParser parser = new DefaultParser();

    public abstract String opt();

    public abstract String description();


    public abstract void process(Channel channel, CommandLine commandLine);

    public void initOptions() {
        options.addOption(opt(), false, description());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(opt());
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }

        if (!(obj instanceof AbstractMonitorOpt)) {
            return false;
        }

        return this.opt() == ((AbstractMonitorOpt) obj).opt();
    }

    public static CommandLine parse(String args[]) throws ParseException {
        return parser.parse(options, args);
    }
}
