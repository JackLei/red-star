/*
 * Copyright (c) 2022 The red-star Project
 *
 * Licensed under the Apache License, version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at:
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.inyourcode.core.monitor;

/**
 * @author JackLei
 */
public interface MetricsConstants {
    String NAME_NODE ="NODE#";

    String NAME_REQUEST_COUNT_PREFIX = "REQUEST_COUNT#";
    String NAME_REQUEST_PROCESS_ALL_TIME = "REQUEST_ALL_TIME#";
    String NAME_REQUEST_PROCESS_SERVER_TIME = "REQUEST_SER_TIME#";
    String NAME_NETWORK_INBOUND = "NETWORK_INBOUND#";
}
